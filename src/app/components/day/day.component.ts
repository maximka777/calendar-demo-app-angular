import { Component, Input, Output, HostListener, EventEmitter } from "@angular/core";

@Component({
    selector: 'app-day',
    templateUrl: './day.component.html',
    styleUrls: ['./day.component.css']
})
export class DayComponent {
    @Input() numberInMonth: number;
    @Input() selected = false;

    @Output() mouseDown = new EventEmitter();
    @Output() mouseUp = new EventEmitter();
    @Output() mouseEnter = new EventEmitter();
    @Output() mouseLeave = new EventEmitter();

    @HostListener('mousedown', ['$event'])
    handleMouseDown(ev) {
        this.mouseDown.emit(this.numberInMonth);
    }

    @HostListener('mouseup', ['$event'])
    handleMouseUp(ev) {
        this.mouseUp.emit(this.numberInMonth);
    }

    @HostListener('mouseenter', ['$event'])
    handleMouseEnter(ev) {
        this.mouseEnter.emit(this.numberInMonth);
    }

    // @HostListener('mouseleave', ['$event'])
    // handleMouseLeave(ev) {
    //     this.mouseLeave.emit(this.numberInMonth);
    // }
}