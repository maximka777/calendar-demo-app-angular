import { Component, HostListener } from "@angular/core";
import { HelperService } from "../../services/helper.service";

@Component({
    selector: 'app-month',
    templateUrl: './month.component.html',
    styleUrls: ['./month.component.css']
})
export class MonthComponent {
    get days() {
        return this.helper.days;
    }

    @HostListener('mouseLeave')
    onMouseLeave() {
        // TODO(max): Add disabling of selection
    }

    isDaySelected(dayNumber) {
        return this.helper.isDaySelected(dayNumber);
    }

    constructor(private helper: HelperService) {
        this.helper.initCurrentMonthDays();
    }

    handleMouseDown(dayNumber) {
        this.helper.selectionModeActive = true;
        console.log('mousedown', dayNumber);
        this.helper.firstSelectedDay = dayNumber;
    }

    handleMouseUp(dayNumber) {
        this.helper.selectionModeActive = false;
        console.log('mouseup', dayNumber);
        this.helper.secondSelectedDay = dayNumber;
    }

    handleMouseEnter(dayNumber) {
        if (this.helper.selectionModeActive) {
            console.log('mouseenter', dayNumber);
            this.helper.secondSelectedDay = dayNumber;
        }
    }
}