import { Injectable } from "@angular/core";
import * as moment from 'moment';
import {range, inRange} from 'lodash';

@Injectable()
export class HelperService {
    selectionModeActive = false;

    private _days: number[];
    set days(value) {
        this._days = value;
    }
    get days() {
        return this._days;
    }
    private _firstSelectedDay: number;
    set firstSelectedDay(value) {
        this._firstSelectedDay = value;
    }
    get firstSelectedDay() {
        return this._firstSelectedDay;
    }
    private _secondSelectedDay: number;
    set secondSelectedDay(value) {
        this._secondSelectedDay = value;
    }
    get secondSelectedDay() {
        return this._secondSelectedDay;
    }

    private getCurrentMonth() {
        const now = moment();
        return 
    }

    initCurrentMonthDays() {
        const countOfDaysInCurrentMonth = moment().daysInMonth();

        // return (function(count) {
        //     return function*() {
        //         let i = 1;
    
        //         while (i <= count) {
        //             yield i++;
        //         }

        //         return i;
        //     };
        // })(countOfDaysInCurrentMonth);

        this.days = range(1, countOfDaysInCurrentMonth + 1);
    }

    isDaySelected(dayNumber) {
        console.log(dayNumber);
        let firstDay = this.firstSelectedDay,
              secondDay = this.secondSelectedDay;
        if (firstDay) {
            if (!secondDay) {
                secondDay = firstDay;
            }   
            
            if (firstDay > secondDay) {
                const temp = firstDay;
                firstDay = secondDay;
                secondDay = temp;
            }
            

            return inRange(dayNumber, firstDay, secondDay + 1);
        }

        return false;
    }   
}
